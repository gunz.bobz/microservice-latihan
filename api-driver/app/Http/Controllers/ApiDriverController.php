<?php

namespace App\Http\Controllers;
use App\Http\Resources\DriverResource;
use App\Http\Resources\DriverResourceCollection;
use Illuminate\Http\Request;
use App\Driver;
class ApiDriverController extends Controller
{

    
    public function show($driver): DriverResource
    {

        $data = Driver::where('id',$driver)->get();
        return new DriverResource($data);
    
    }
    // public function show(Driver $driver): DriverResource
    // {
    //     return new DriverResource($driver);
    
    // }

    public function index():DriverResourceCollection 
    {
        return new DriverResourceCollection(Driver::paginate());
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required',
            'tanggallahir' => 'required|before:today|after:1900-01-01',
            'jk' => 'required',
            'alamat' => 'required',
            'villages' => 'required',
            'districts' => 'required',
            'cities' => 'required',
            'provinces' => 'required',
            'email' => 'unique:drivers|required',
            'telp' => 'unique:drivers|required|digits_between:8,13',
            ]);  
               
            return $request->all();
            $driver=Driver::create($request->all());
            return new DriverResource($driver);
    }
    public function update(Request $request,$driver): DriverResource
    {
        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required',
            'tanggallahir' => 'required|before:today|after:1900-01-01',
            'jk' => 'required',
            'alamat' => 'required',
            'villages' => 'required',
            'districts' => 'required',
            'cities' => 'required',
            'provinces' => 'required',
            'email' => 'unique:drivers|required',
            'telp' => 'unique:drivers|required|digits_between:8,13',
            ]);  
               
            // return $request->all();
            
            $data=Driver::where('id',$driver)->first();
            // $data->nama = $request->input('nama');
            // $data->save();        
            $data->update($request->all());
            return new DriverResource($data);        
    }
    public function destroy($driver)
    {
        $data = Driver::where('id',$driver);
        $data->delete();
            return response()->json();        
    }
}
