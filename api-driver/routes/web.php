<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('driver/','ApiDriverController@index');
$router->post('driver/','ApiDriverController@store');
$router->get('driver/{driver}','ApiDriverController@show');
$router->delete('driver/{driver}','ApiDriverController@destroy');
$router->patch('driver/{driver}','ApiDriverController@update');

