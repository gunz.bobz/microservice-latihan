<?php

namespace App\Http\Controllers;
use App\Http\Resources\ValetResource;
use App\Http\Resources\ValetResourceCollection;
use Illuminate\Http\Request;
use App\Valet;
class ApiValetController extends Controller
{
    public function confirm(Request $request,$barcode):ValetResource
    {
        
        $this->validate($request, [
            'driver_id'=>'required',
            ]);    

            $vale = new Valet;
            $vale->driver_id = $request->driver_id;;
            $vale->barcode_valet = $barcode;
            $vale->save();
            return new ValetResource($vale);
            // $valet->driver_id=$data;
            // $valet->save();
            // $data->barcode_valet = $barcode;
            // $data=Valet::create($request->all());
            // $data->save();
        // return $barcode.'-_-'.$request;
        // return $request->driver_id;
        
    }
}
