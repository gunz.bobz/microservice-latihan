<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Valet extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    protected $table='valets';
}
