<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    @include('layout.style')

    </head>

    <body>  
            <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-company-red mb-3 " id="nav" >
                <a class="navbar-brand pr-5" href="#">
                    <img src="/favicon/valet.png" width="50" height="50" alt="">
                </a>
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav " id="nav-link">
                        <li class="nav-item ">
                            <a class="nav-link mr-3" href="/menus">Home </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-3" href="/menus" >Edit Landing Page Menus</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-3" href="/valets" >Valet Capacity</a>
                        </li>
                    </ul>
                </div>  
            </nav>
        
           
                
                @yield('content')   
         
                @include('layout.js') 
                
        
        </body>
</html>
            
                
                