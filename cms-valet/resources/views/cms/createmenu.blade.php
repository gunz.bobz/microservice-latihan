@extends('layout/app')

@section('title','Form Pendaftaran Pasien')

@section('content')

    

    <div class="row mx-auto pt-3 pb-3 justify-content-center ">
        <div class="col-10 ">       

            <form action="/menus/create" method="POST" enctype="multipart/form-data">
             

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control " id="title" placeholder="Masukan title" name="title" value="">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea type="text" class="form-control " id="description" placeholder="Masukan description" name="description" value=""></textarea>
            </div>
            <div class="form-group ">   
                <b>File Foto</b><br/>
                <input type="file" name="file" id="imgInp" class="btn bg-company-red  text-white">
                <div class="d-flex justify-content-center align-items-center container ">
                <img class="foto justify-content-center" id="blah" src="/images/default/nophoto.png"   alt="your image" style="max-height:200px max-width:200px "/>
                </div>
            </div>                 
            <div class="col-md-12">
                <button type="submit" class="mb-5 btn text-white  bg-company-red float-md-right">Tambahkan Data!</button>
            </div>

            </form>
     
    </div>
    </div> 
    
    @endsection
    
    @section('scripts')
    <script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function () {
        readURL(this);
    });
</script>    
    @endsection
