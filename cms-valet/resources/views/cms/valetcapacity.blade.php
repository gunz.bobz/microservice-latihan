@extends('layout/app')

@section('title','Edit Menu')

@section('content')
<div class="row mx-auto pt-3 pb-3 d-flex justify-content-center ">
        <div class="col-md-8"> 
            <h1 class="mt-3 text-center">Valet Capacity</h1>
            <table class="table table-hover  table-responsive-md">
                    <thead class='text-white  bg-company-red'>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Name</th>
                            <th scope="col">Capacity</th>
                            <th scope="col">Manage</th>
                            
                            
                        </tr>
                </thead>
                        <tbody>
                            <?php $count = 1; ?> 
                            @foreach ($valets as $valet)    
                            <tr>
                                <th scope="row">{{ $valets ->perPage()*($valets->currentPage()-1)+$count ?? ''}}</th>
                                <td>{!! $valet->nama !!}</td>
                                <td>{!! $valet->capacity !!}</td>
                                <td>
                                    <form action="{{ url('valets/'.$valet->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="DELETE" class="form-control">
                                    <button class="btn btn-danger btn-sm">delete {{ url('valets/'.$valet->id) }}</button>
                                    <input type="hidden" value="{{ $valet->id }}" name="id"></form>
                                    <form action="{{ url('valets/'.$valet->id.'/edit') }}" method="GET">
                                    <button class="btn btn-info btn-sm">edit {{ url('valets/'.$valet->id.'/edit') }}</button>
                                    </form>
                                </td>            
                                </td> 
                            </tr>
                            <?php $count ++; ?>
                            @endforeach                       
                        </tbody>   
            </table>   
        

  
    <div class="row mx-auto pt-3 pb-3 justify-content-center ">
       <a href="/valets/create"> <button type="button" class="btn bg-company-red text-white" data-toggle="modal" data-target="#modalMenus">
            Add Zone
          </button>
        </a>
  </div>





        </div>
</div>
@endsection
