@extends('layout/app')

@section('title','Form Pendaftaran Pasien')

@section('content')

    

    <div class="row mx-auto pt-3 pb-3 justify-content-center ">
        <div class="col-10 ">       

            <form action="/valets/create" method="POST" enctype="multipart/form-data">
             

            <div class="form-group">
                <label for="zone">Zone</label>
                <input type="text" class="form-control " id="zone" placeholder="Masukan zone" name="zone" value="">
            </div>

            <div class="form-group">
                <label for="capacity">Capacity</label>
                <input type="text" class="form-control " id="capacity" placeholder="Masukan capacity" name="capacity" value="">
            </div>
            <div class="col-md-12">
                <button type="submit" class="mb-5 btn text-white  bg-company-red float-md-right">Tambahkan Data!</button>
            </div>

            </form>
     
    </div>
    </div> 
    
    @endsection
    
    @section('scripts')
    <script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function () {
        readURL(this);
    });
</script>    
    @endsection
