@extends('layout/app')
@section('title','Edit Menu')
@section('content')
<div class="row mx-auto pt-3 pb-3 d-flex justify-content-center ">
    <div class="col-md-12"> 
        <h1 class="mt-3 text-center">Menus</h1>
            <table class="table table-hover  table-responsive-md">
                <thead class='text-white  bg-company-red  '>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">title</th>
                        <th scope="col">description</th>
                        <th scope="col">image</th>
                        <th scope="col">delete</th>
                    </tr>
                </thead>
                    <tbody>
                        <?php $count = 1; ?> 
                        @foreach ($menus as $menu)    
                            <tr>
                                <th scope="row">{{ $menus ->perPage()*($menus->currentPage()-1)+$count ?? ''}}</th>
                                <td>{!! $menu->title !!}</td>
                                <td>{!! $menu->desc !!}</td>
                                <td><img src="images/{{ $menu->image }}"  onerror="this.onerror=null; this.src='/images/default/nophoto.png'"style="width:200px; height:auto;"></td>
                                <td>
                                    <form action="{{ url('menus/'.$menu->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="DELETE" class="form-control">
                                    <button class="btn btn-danger btn-sm">delete {{ url('menus/'.$menu->id) }}</button>
                                    <input type="hidden" value="{{ $menu->id }}" name="id"></form>
                                    <form action="{{ url('menus/'.$menu->id.'/edit') }}" method="GET">
                                    <button class="btn btn-info btn-sm">edit {{ url('menus/'.$menu->id.'/edit') }}</button>
                                    </form>
                                </td> 
                            </tr>
                        <?php $count ++; ?>
                        @endforeach                       
                </tbody>   
            </table>   
<div class="row mx-auto pt-3 pb-3 justify-content-center ">
    <a href="/menus/create"> <button type="button" class="btn bg-company-red text-white" data-toggle="modal" data-target="#modalMenus">
        Add Menus
        </button>
    </a>
</div>
</div>
</div>
@endsection
