<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//index kunjungan
$router->get('/menus', 'MenuController@index');
$router->delete('/menus/{id}', 'MenuController@destroy');
$router->patch('/menus/{id}', 'MenuController@update');
$router->get('/menus/create', 'MenuController@create');
$router->get('/menus/{menus}/edit', 'MenuController@edit');
$router->post('/menus/create', 'MenuController@store');
$router->get('/valets', 'ValetController@index');
$router->get('/valets/create', 'ValetController@create');
$router->post('/valets/create', 'ValetController@store');
$router->delete('/valets/{id}', 'ValetController@destroy');
$router->get('/valets/{valets}/edit', 'ValetController@edit');
$router->patch('/valets/{id}', 'ValetController@update');
$router->get('/menus', 'MenuController@index');
// $router->delete('/menus/{id}', 'MenuController@destroy');
// $router->patch('/menus/{id}', 'MenuController@update');
// $router->get('/menus/create', 'MenuController@create');
$router->put('api/menus/{menu}', 'Api\ApiMenuController@update');
$router->post('api/menus/create', 'Api\ApiMenuController@store');
$router->get('api/menus', 'Api\ApiMenuController@index');
$router->get('api/menus/{menu}', 'Api\ApiMenuController@show');
// $router->get('/valets/create', 'ValetController@create');
$router->post('api/valets/create', 'ApiValetController@store');
// $router->delete('api/valets/{id}', 'ApiValetController@destroy');
// $router->get('/valets/{valets}/edit', 'ValetController@edit');
// $router->patch('/valets/{id}', 'ValetController@update');