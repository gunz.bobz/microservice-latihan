<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Valet;
use Image;
use File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ValetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index()
    {
        // echo 'dsad';
        $valets = Valet::paginate(15);
        // dd($valets);
        return view('cms.valetcapacity',compact('valets'));
    }
    public function create()
    {
        // echo 'dsad';
        // $valets = Valet::paginate(15);
        // dd($valets);
        return view('cms.createvalet');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'zone' => 'required',
            'capacity' => 'required',
            ]);  
            $valet = new Valet;
               
                    $valet->nama = $request->zone;
                    $valet->capacity = $request->capacity;
                    $valet->save();
                    return redirect('/valets') ;
    }
    public function destroy($id)
    {
        // return $id;
        $valet = Valet::find($id);
        Valet::destroy($valet->id);
        return redirect('/valets'); 

    }
    public function edit($valets)
    {
        // return $valets;
        // $valet = Valet::all()
        // ->where('id', '=', $valets)->get();
        $valet = Valet::find($valets);
        // return $valet;
        return view('cms.editvalet',compact('valet'));
    }
    public function update(Request $request, Valet $valets)
    {
       

        $this->validate($request, [
            'zone' => 'required',
            'capacity' => 'required',
            ]);  
            $valet = new Valet;
            
                // dd($request->id);
                Valet::where('id',$request->id)
                    ->update([
                      
                        'nama' => $request->zone,
                        'capacity' => $request->capacity,
                        ]);          
                        return redirect('/valets') ;
                    }
}
