<?php

namespace App\Http\Controllers\Api;
use App\Resources\MenuResource;
use App\Resources\MenuResourceCollection;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Menu;
// use Str;

class ApiMenuController extends Controllers\Controller
{
   
    public function index():MenuResourceCollection
    {
      
      
      //  echo "dsad0";
      // $data2 = Menu::with('menu')->get();
        // $data = Menu::with('menu')->where('driver_id',$id_driver)->with('history')->get();
        $data = Menu::all();
        $data->makeHidden(['deleted_at','created_at','updated_at']);  

        return new MenuResourceCollection($data);
      }
  
      public function show($menu): MenuResource
      {
          // $menu = Menu::findOrFail($menu);
          
          
          $data = Menu::where('id',$menu)->get();
          $data->makeHidden(['completed_at','updated_at']);

          return new MenuResource($data);
      
      }
    public function store(Request $request): MenuResource
    {
      // dd($request->all());
      // return $request;
      

      // dd($data->all());
      //   $this->validate($request, [
        //       'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //       'title'=>'required',
        //       'desc'=>'required',
        //       ]);  
        // $file = $request->file('image');
        // $filename = date('mdYHis') .'_'. uniqid() .'.'.$file->getClientOriginalExtension();
        // $folder ='images';
        // $file->move($folder,$filename);
        
            $menu = new Menu;
            $menu->image=$request->filename;
            $menu->title = $request->title;
            $menu->desc = $request->desc;
            $menu->save();
            
            return new MenuResource($menu);
    }
  
   
    public function update (Request $request,$menu)
    // : MenuResource 
    {
      // return $request['image'];
      // $post = $request->all();
      // dd($request);
      // print_r($request->all());die();
      // dd(($request)->getBody());
      
      // dd($request());
      // return 'dsad';
      // echo 'dsad';



    // return $request;
 
      // $this->validate($request, [
      //   'image'=>'required',
      //   'title'=>'required',
      //   'desc'=>'required',
      //   ]);  

        // $file = $request->file('image');
        // $filename = date('mdYHis') .'_'. uniqid() .'.'.$file->getClientOriginalExtension();
        // $folder ='images';
        // $file->move($folder,$filename);
        // dd($filename);           
      // echo $menu;
      $id=$menu;
      // $data = Menu::where('id',$menu)->toSql();
      var_dump($id);
      // dd($menu);
      // $data->update($request);

      // $data->image = $request->image;
      // $data->title = $request ->title;
      // $data->desc = $request ->desc;
      // $data->save();        
      // return new MenuResource($data);        
    }
    public function destroy($menu)
    {
        $data = Menu::where('id',$menu);
        $data->delete();
        return response()->json();        
    }
}
