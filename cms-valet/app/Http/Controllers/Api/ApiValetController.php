<?php

namespace App\Http\Controllers;
use App\Resources\ValetResource;
use App\Resources\ValetResourceCollection;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Valet;

class ApiValetController extends Controller
{
   
    
  

    public function store(Request $request):ValetResource
    {
        $this->validate($request, [
            'nama'=>'required',
            'capacity'=>'required',
            ]);  
   
            $Valet=Valet::create($request->all());
            return new ValetResource($Valet);
    }
  
   
   
    public function destroy($Valet)
    {
        $data = Valet::where('id',$Valet);
        $data->delete();
            return response()->json();        
    }
}
