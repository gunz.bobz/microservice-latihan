<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Image;
use File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index()
    {
        // echo 'dsad';
        $menus = Menu::paginate(15);
        // dd($menus);
        return view('cms.indexmenu',compact('menus'));
    }
    public function create()
    {
        // echo 'dsad';
        // $menus = Menu::paginate(15);
        // dd($menus);
        return view('cms.createmenu');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required',
            'description' => 'required',
            ]);  
                $menu = new Menu;
                if($request->has('file'))
                {
                    $file = $request->file('file');
                    $filename = date('mdYHis') .'_'. uniqid() .'.'.$file->getClientOriginalExtension();
                    $folder ='images';
                    $file->move($folder,$filename);
                }else{
                    $filename = "default.png";
                } 
                    $menu->image = $filename;
                    $menu->title = $request->title;
                    $menu->desc = $request->description;
                    $menu->save();
                    return redirect('/menus') ;
    }
    public function destroy($id)
    {
        // return $id;
        $menu = Menu::find($id);
        Menu::destroy($menu->id);
        return redirect('/menus'); 

    }
    public function edit($menus)
    {
        // return $menus;
        // $menu = Menu::all()
        // ->where('id', '=', $menus)->get();
        $menu = Menu::find($menus);
        // return $menu;
        return view('cms.editmenu',compact('menu'));
    }
    public function update(Request $request, Menu $menus)
    {
       

        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required',
            'description' => 'required',
            ]);  
            $menu = new Menu;
            if($request->has('file'))
            {
                $folder ='images';
                $image =$folder.'/'.$menus->foto;
                // if(File::exists($image))
                // {
                // File::delete($image);
                // }
                $file = $request->file('file');
                $filename = date('mdYHis') .'_'. uniqid() .'.'.$file->getClientOriginalExtension();
                $file->move($folder,$filename);
            }else 
            {
                $filename = 'default.png';
            } 
                // dd($request->id);
                Menu::where('id',$request->id)
                    ->update([
                        'image' => $filename,
                        'title' => $request->title,
                        'desc' => $request->description,
                        ]);          
                        return redirect('/menus') ;
                    }
}
