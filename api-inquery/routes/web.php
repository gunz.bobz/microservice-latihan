<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('usedvalet','ApiInqueryController@usedvalet');
$router->get('totalvalet','ValetController@totalvalet');


$router->get('inquery/{inquery}','ApiInqueryController@show');
$router->post('inquery/','ApiInqueryController@store');
$router->put('inquery/{inquery}','ApiInqueryController@barcodescan');
$router->put('scan/{inquery}','ApiInqueryController@barcodescan');
$router->put('inquery/{inquery}/checkout','ApiInqueryController@checkout');
$router->put('inquery/{inquery}/cancel','ApiInqueryController@cancel');