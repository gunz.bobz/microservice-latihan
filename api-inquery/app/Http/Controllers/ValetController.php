<?php

namespace App\Http\Controllers;
use App\Http\Resources\InqueryResource;
use App\Http\Resources\InqueryResourceCollection;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\TotalValet;
class ValetController extends Controller
{
 
    public function totalvalet(Request $request)
    {
        $data = TotalValet::all()->sum('capacity');
        // $data->makeHidden(['completed_at']);
        // return $this->successResponse($data);
        return json_decode($data);
    }

}
