<?php

namespace App\Http\Controllers;
use App\Http\Resources\InqueryResource;
use App\Http\Resources\InqueryResourceCollection;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Inquery;

class ApiInqueryController extends Controller
{
    use ApiResponser;
    
    public function show($inquery)
    {
        // $inquery = Inquery::findOrFail($inquery);
        
        
        $data = Inquery::where('barcode',$inquery)
        ->with('getstatus')->with('getnama')->get();
        $data->makeHidden(['completed_at','updated_at']);
        return $this->successResponse($data);
        // return new InqueryResource($data);
        
        // ->where('status_id','1')
    }
    // public function show(Inquery $inquery): InqueryResource
    // {
    //     return new InqueryResource($inquery);
    
    // }

    public function usedvalet(Request $request)
    {
        $data = Inquery::where('status_id','2')->count();
        // $data->makeHidden(['completed_at']);
        // return $this->successResponse($data);
        return json_decode($data);
    }


    public function store(Request $request):InqueryResource
    {
        $this->validate($request, [
            'id_customer'=>'required',
            'nama_kendaraan'=>'required',
            'warna_kendaraan'=>'required',
            'nomor_polisi'=>'required',
            ]);  
            // return $request->all();

            $inquery=Inquery::create($request->all());
            // return toArray($inquery);
            return new InqueryResource($inquery);
    }
    public function barcodescan(Request $request,$barcode): InqueryResource
    {
     
        $data=Inquery::where('barcode',$barcode)->first();
            $data->status_id = '2';
            $data->update();        
            $res =Inquery::where('barcode',$barcode)->with('getstatus')->with('getnama')->first();
            // $data->update($request->all());
            return new InqueryResource($res);        
    }
    public function checkout(Request $request,$inquery): InqueryResource
    {
        $this->validate($request, [
            // 'status_id' => 'required',
            ]);  
            // return $request->all();

            $data=Inquery::where('barcode',$inquery)->where('status_id','2')->with('valet.getnama')
            ->with('valet.getstatus')->findOrFail();
            
            $data->status_id = '3';
            $data->save();        
            // $data->update($request->all());
            return new InqueryResource($data);        
    }
    public function cancel(Request $request,$inquery): InqueryResource
    {
        $this->validate($request, [
            // 'status_id' => 'required',
            ]);  
            // return $request->all();
            $data=Inquery::where('barcode',$inquery)->first();
            $data->status_id = '4';
            $data->save();        
            $res=Inquery::where('barcode',$inquery)->with('getstatus')->with('getnama')->first();
            // $data->update($request->all());
            return new InqueryResource($res);        
    }
    public function destroy($inquery)
    {
        $data = Inquery::where('id',$inquery);
        $data->delete();
            return response()->json();        
    }
}
