<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inquery extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    protected $hidden=['completed_at','updated_at','deleted_at','id'];
    public function getstatus()
    {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }    
    public function getnama()
    {
        return $this->hasOne('App\Customer', 'customer_id', 'id_customer');
    }    
}
