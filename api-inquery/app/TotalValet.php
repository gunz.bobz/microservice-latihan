<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class TotalValet extends Model
{
    // use SoftDeletes;
    protected $guarded=[];
    protected $table='total_valet';

}
