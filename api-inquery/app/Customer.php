<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    // use SoftDeletes;
    protected $table='customers';
    protected $guarded=[];
    protected $hidden=['id','customer_id','created_at','updated_at'];


}
