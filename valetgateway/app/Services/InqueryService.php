<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class InqueryService
{
    use ConsumeExternalService;

    /**
     * The base uri to consume inqueries service
     * @var string
     */
    public $baseUri;

    /**
     * Inqueryization secret to pass to inquery api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.inqueries.base_uri');
        $this->secret = config('services.inqueries.secret');
    }


    /**
     * Obtain the full list of inquery from the inquery service
     */
    public function obtainInqueries()
    {
        return $this->performRequest('GET', '/inquery');
    }

    /**
     * Create Inquery
     */
    public function createInquery($data)
    {
        return $this->performRequest('POST', env("INQUERIES_SERVICE_BASE_URL")."/inquery/", $data);
    }

    /**
     * Get a single inquery data
     */
    public function obtainInquery($inquery)
    {
        return $this->performRequest('GET', "/inquery/{$inquery}");
    }

    /**
     * Edit a single inquery data
     */
    public function editInquery($data, $inquery)
    {
        return $this->performRequest('PUT', "/inqueries/{$inquery}", $data);
    }

    /**
     * Delete an Inquery
     */
    public function deleteInquery($inquery)
    {
        return $this->performRequest('DELETE', "/inqueries/{$inquery}");
    }
}