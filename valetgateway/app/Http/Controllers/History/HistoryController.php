<?php

namespace App\Http\Controllers\History;


use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
class HistoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show a single inquery details
     * @param $inquery
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id_driver)
    {
        return $this->Get(env('HISTORIES_SERVICE_BASE_URL').'/history/'.$id_driver);
    }

}
