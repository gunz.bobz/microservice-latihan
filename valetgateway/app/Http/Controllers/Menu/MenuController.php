<?php

namespace App\Http\Controllers\Menu;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class MenuController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        return $this->Get(env('CMS_SERVICE_BASE_URL').'/api/menus');
    }
   
    public function show($menus)
    {
        return $this->Get(env('CMS_SERVICE_BASE_URL').'/api/menus/'.$menus);
    }
   


    
    public function store(Request $request)
    { 
        $file = $request->file('image');           
        $filename = date('mdYHis') .'_'. uniqid() .'.'.$file->getClientOriginalExtension();
        $folder ='images';
        $file->move($folder,$filename);
        $title=$request->title;
        $desc=$request->desc;
        $res['title'] = $title;
        $res['desc'] = $desc;
        $res['filename'] = $filename;
        // dd($filename);
        return $this->Post(env('CMS_SERVICE_BASE_URL').'/api/menus/create/',$res);
    }
    public function update(Request $request,$menu)
    { 
        $file = $request->file('image');           
        $filename = date('mdYHis') .'_'. uniqid() .'.'.$file->getClientOriginalExtension();
        $folder ='images';
        // $file->move($folder,$filename);
    //    return $filename;
        $data=[
            'multipart'=>   [
                [
                    'name'     => 'title',
                    'contents' => $request->title
                ],
                [
                    'name'     => 'desc',
                    'contents' => $request->desc
                ],
                [
                    'name'     => 'image',
                    'contents' => $filename
                ],
                [
                    'name'     => 'id',
                    'contents' => $menu
                ],
                [
                    'name'     => '_method',
                    'contents' => 'PUT'
                ],
            ]
            ];

        $client = new Client();
        $response=$client->Post(env('CMS_SERVICE_BASE_URL').'/api/menus/'.$menu,$data);
        // return Json_decode($response->getBody(),true);
      
    
    
    }
}
