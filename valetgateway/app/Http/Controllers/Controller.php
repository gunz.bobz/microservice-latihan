<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
class Controller extends BaseController
{
    protected function Get($url)
    {
        $client = New CLient();
        $reqGet = $client->get($url);
        $resGet = json_decode($reqGet->getBody(),true);
        return $resGet;
    }
    protected function Post($url,$data)
    {
        $client = new Client();
        $reqPost = $client->post($url,[
            'form_params'=>$data
        ]);
        $resPost = json_decode($reqPost->getBody(),true);
        return $resPost;
    }
    protected function put1($url,$data)
    {
        $client = new Client();
        $reqPost = $client->put($url,[
            'multipart'=>$data
        ]);
        $resPost = json_decode($reqPost->getBody(),true);
        return $resPost;
    }

    protected function scan($url,$data)
    {
      $client = new GuzzleHttp\Client();$res = $client->request(‘PUT’, $url);

    }
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            // 'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }
}
