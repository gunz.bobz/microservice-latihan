<?php

namespace App\Http\Controllers\Inquery;

use App\Http\Controllers\Controller;
use App\Services\InqueryService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class InqueryController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the inquery micro-service
     * @var InqueryService
     */
    public $inqueryService;

    public function __construct()
    {
        $this->middleware('auth');
    }



 

   

    /**
     * Show a single inquery details
     * @param $inquery
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($inquery)
    {
        // $client = new Client();
        // return $client;
        // return $response;
        // $response = $client->get(['base_uri' => 'http://localhost:8001/inquery/'.$inquery]) ;
        // return $this->successResponse($this->inqueryService->obtainInquery($inquery));

       return $this->Get(env('INQUERIES_SERVICE_BASE_URL').'/inquery/'.$inquery);
    }
    public function usedvalet()
    {

       return $this->Get(env('INQUERIES_SERVICE_BASE_URL').'/usedvalet/');
    }
    public function totalvalet()
    {

       return $this->Get(env('INQUERIES_SERVICE_BASE_URL').'/totalvalet/');

    }
    
    public function availablevalet()
    {
        $total=$this->Get(env('INQUERIES_SERVICE_BASE_URL').'/totalvalet/');
        $used=$this->Get(env('INQUERIES_SERVICE_BASE_URL').'/usedvalet/');
        $avail=$used."/".$total;
       return  $avail;
       }


    public function store(Request $request)
    {
        $data=$request->all();
        // return $data;
        return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/inquery/',$data);
        
    }

    public function barcodescan($barcode)
    {
    
        $client = new Client();
        $response= $client->request('PUT',env('INQUERIES_SERVICE_BASE_URL').'/inquery/'.$barcode);
        return json_decode($response->getBody(),true);
        // return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/inquery/',$data);
    }

    public function cancel($barcode)
    {
    
        $client = new Client();
        $response= $client->request('PUT',env('INQUERIES_SERVICE_BASE_URL').'/inquery/'.$barcode.'/cancel');
        return json_decode($response->getBody(),true);
        // return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/inquery/',$barcode.'/cancel');
    }
    public function checkout($barcode)
    {
    
        $client = new Client();
        $response= $client->request('PUT',env('INQUERIES_SERVICE_BASE_URL').'/inquery/'.$barcode.'/checkout');
        return json_decode($response->getBody(),true);
        // return $this->Post(env('INQUERIES_SERVICE_BASE_URL').'/inquery/',$barcode.'/cancel');
    }


}
