<?php

namespace App\Http\Controllers\Valet;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;


class ValetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function confirm(Request $request,$barcode)
    {

        $data=$request->all();
        // $data2=[$data,$barcode];
        // return, $data;
        return $this->Post(env('VALETS_SERVICE_BASE_URL').'/valet/'.$barcode,$data );
        

        // $client = new Client();
        // $response= $client->request('POST',env('VALETS_SERVICE_BASE_URL').'/valet/'.$barcode);
        // dd($response);
        
        // dd(env('VALETS_SERVICE_BASE_URL').'/valet/'.$barcode));



        // return json_decode($response->getBody(),true);
        // return $this->Post(env('VALETS_SERVICE_BASE_URL').'/valet/',$data);

        // $client = new Client();
        // $response= $client->request('POST',env('VALETS_SERVICE_BASE_URL').'/valet/',$barcode);
        // return json_decode($response->getBody(),true);
        // return $this->Post(env('VALETS_SERVICE_BASE_URL').'/valet/',$barcode);
        // echo env('VALETS_SERVICE_BASE_URL').'/valet/'.$barcode;
    }

    public function destroy($id)

    {
        $client = new Client();
        $response=$client->delete(env('CMS_SERVICE_BASE_URL').'/api/valets/'.$id);
        return json_decode($response->getBody(),true);
    }

}
