<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Vehicle::class, function (Faker\Generator $faker) {
    return [
        'id_pemilik' => $faker-> numberBetween($min = 1, $max = 1111111111),
        'foto_kendaraan' => $faker->imageUrl($width = 640, $height = 480, 'transport'),
        'jenis_kendaraan' => $faker-> randomElement(['Sedan', 'mini bus','truck','bus']),
        'warna_kendaraan' => $faker->safeColorName,
        'merk_kendaraan' => $faker-> randomElement(['toyota', 'honda','porsche']),
        'nomor_polisi' => $faker-> randomElement(['D XXXX TR', 'B 1 SA']),
        'tipe_kendaraan' => $faker-> randomElement(['Jazz', 'inova']),
        'tahun_kendaraan' =>$faker-> numberBetween($min = 1977, $max = 2019),
    ];
});
