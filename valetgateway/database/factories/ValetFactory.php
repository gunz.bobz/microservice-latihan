<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Valet;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Valet::class, function (Faker $faker) {
    return [
        'id_driver' => $faker->isbn10,
        'barcode_valet' => $faker-> numberBetween($min = 1, $max = 1111111111),
    
   ];
});
