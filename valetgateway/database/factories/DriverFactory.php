<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Driver;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Driver::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'nomor' => $faker->isbn10,
        'foto' => $faker->imageUrl($width = 640, $height = 480),
        'tanggallahir' => $faker-> date(),
        'jk' => $faker-> randomElement(['laki-laki', 'perempuan']),
        'alamat' => $faker-> streetAddress,
        'cities' => $faker-> city,
        'districts' => $faker-> cityPrefix,
        'villages' => $faker-> country,
        'provinces' => $faker-> state,
        'telp' => $faker-> phoneNumber,
        'email' => $faker-> safeEmail,
   ];
});
