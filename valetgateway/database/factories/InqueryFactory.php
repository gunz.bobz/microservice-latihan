<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Inquery::class, function (Faker\Generator $faker) {
    return [
        'barcode' => $faker-> isbn10,
        'nama_kendaraan' => $faker->randomElement(['honda jazz', 'suzuki expander']),
        'warna_kendaraan' => $faker->randomElement(['hitam', 'putih','merah','kuning']),
        'nomor_polisi' => $faker-> randomElement(['D XXXX TR', 'B 1 SA']),
        'status' => $faker-> randomElement(['1', '2', '3', '4']),
        'id_customer' => $faker-> isbn10,
    ];
});
