<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('status')->delete();
        
        \DB::table('status')->insert(array (
            0 => 
            array (
                'id' => '1',
                'status' => 'In Process',
            ),
            1 => 
            array (
                'id' => '2',
                'status' => 'Parked',
                
            ),
            2 => 
            array (
                'id' => '3',
                'status' => 'Completed',
                
            ),
            3 => 
            array (
                'id' => '4',
                'status' => 'Canceld',
                
            ),
            4 => 
            array (
                'id' => '5',
                'status' => 'other',
                
            ),
        ));
        
        
    }
}