<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'id' => '11',
                'name' => 'ACEH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '12',
                'name' => 'SUMATRA UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '13',
                'name' => 'SUMATRA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '14',
                'name' => 'RIAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '15',
                'name' => 'JAMBI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '16',
                'name' => 'SUMATRA SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '17',
                'name' => 'BENGKULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '18',
                'name' => 'LAMPUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '19',
                'name' => 'KEPULAUAN BANGKA BELITUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '21',
                'name' => 'KEPULAUAN RIAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '31',
                'name' => 'DKI JAKARTA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '32',
                'name' => 'JAWA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '33',
                'name' => 'JAWA TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '34',
                'name' => 'DI YOGYAKARTA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '35',
                'name' => 'JAWA TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '36',
                'name' => 'BANTEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '51',
                'name' => 'BALI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '52',
                'name' => 'NUSA TENGGARA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '53',
                'name' => 'NUSA TENGGARA TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '61',
                'name' => 'KALIMANTAN BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '62',
                'name' => 'KALIMANTAN TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '63',
                'name' => 'KALIMANTAN SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '64',
                'name' => 'KALIMANTAN TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '65',
                'name' => 'KALIMANTAN UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '71',
                'name' => 'SULAWESI UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '72',
                'name' => 'SULAWESI TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '73',
                'name' => 'SULAWESI SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '74',
                'name' => 'SULAWESI TENGGARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '75',
                'name' => 'GORONTALO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '76',
                'name' => 'SULAWESI BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '81',
                'name' => 'MALUKU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '82',
                'name' => 'MALUKU UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '91',
                'name' => 'PAPUA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '94',
                'name' => 'PAPUA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}