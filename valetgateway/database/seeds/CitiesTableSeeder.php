<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => '1101',
                'province_id' => '11',
                'name' => 'KABUPATEN SIMEULUE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '1102',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH SINGKIL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '1103',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '1104',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH TENGGARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '1105',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '1106',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '1107',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '1108',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH BESAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '1109',
                'province_id' => '11',
                'name' => 'KABUPATEN PIDIE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '1110',
                'province_id' => '11',
                'name' => 'KABUPATEN BIREUEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '1111',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '1112',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH BARAT DAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '1113',
                'province_id' => '11',
                'name' => 'KABUPATEN GAYO LUES',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '1114',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH TAMIANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '1115',
                'province_id' => '11',
                'name' => 'KABUPATEN NAGAN RAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '1116',
                'province_id' => '11',
                'name' => 'KABUPATEN ACEH JAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '1117',
                'province_id' => '11',
                'name' => 'KABUPATEN BENER MERIAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '1118',
                'province_id' => '11',
                'name' => 'KABUPATEN PIDIE JAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '1171',
                'province_id' => '11',
                'name' => 'KOTA BANDA ACEH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '1172',
                'province_id' => '11',
                'name' => 'KOTA SABANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '1173',
                'province_id' => '11',
                'name' => 'KOTA LANGSA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '1174',
                'province_id' => '11',
                'name' => 'KOTA LHOKSEUMAWE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '1175',
                'province_id' => '11',
                'name' => 'KOTA SUBULUSSALAM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '1201',
                'province_id' => '12',
                'name' => 'KABUPATEN NIAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '1202',
                'province_id' => '12',
                'name' => 'KABUPATEN MANDAILING NATAL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '1203',
                'province_id' => '12',
                'name' => 'KABUPATEN TAPANULI SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '1204',
                'province_id' => '12',
                'name' => 'KABUPATEN TAPANULI TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '1205',
                'province_id' => '12',
                'name' => 'KABUPATEN TAPANULI UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '1206',
                'province_id' => '12',
                'name' => 'KABUPATEN TOBA SAMOSIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '1207',
                'province_id' => '12',
                'name' => 'KABUPATEN LABUHAN BATU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '1208',
                'province_id' => '12',
                'name' => 'KABUPATEN ASAHAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '1209',
                'province_id' => '12',
                'name' => 'KABUPATEN SIMALUNGUN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '1210',
                'province_id' => '12',
                'name' => 'KABUPATEN DAIRI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '1211',
                'province_id' => '12',
                'name' => 'KABUPATEN KARO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => '1212',
                'province_id' => '12',
                'name' => 'KABUPATEN DELI SERDANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => '1213',
                'province_id' => '12',
                'name' => 'KABUPATEN LANGKAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => '1214',
                'province_id' => '12',
                'name' => 'KABUPATEN NIAS SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => '1215',
                'province_id' => '12',
                'name' => 'KABUPATEN HUMBANG HASUNDUTAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => '1216',
                'province_id' => '12',
                'name' => 'KABUPATEN PAKPAK BHARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => '1217',
                'province_id' => '12',
                'name' => 'KABUPATEN SAMOSIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => '1218',
                'province_id' => '12',
                'name' => 'KABUPATEN SERDANG BEDAGAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => '1219',
                'province_id' => '12',
                'name' => 'KABUPATEN BATU BARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => '1220',
                'province_id' => '12',
                'name' => 'KABUPATEN PADANG LAWAS UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => '1221',
                'province_id' => '12',
                'name' => 'KABUPATEN PADANG LAWAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => '1222',
                'province_id' => '12',
                'name' => 'KABUPATEN LABUHAN BATU SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => '1223',
                'province_id' => '12',
                'name' => 'KABUPATEN LABUHAN BATU UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => '1224',
                'province_id' => '12',
                'name' => 'KABUPATEN NIAS UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => '1225',
                'province_id' => '12',
                'name' => 'KABUPATEN NIAS BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => '1271',
                'province_id' => '12',
                'name' => 'KOTA SIBOLGA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => '1272',
                'province_id' => '12',
                'name' => 'KOTA TANJUNG BALAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => '1273',
                'province_id' => '12',
                'name' => 'KOTA PEMATANG SIANTAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => '1274',
                'province_id' => '12',
                'name' => 'KOTA TEBING TINGGI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => '1275',
                'province_id' => '12',
                'name' => 'KOTA MEDAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => '1276',
                'province_id' => '12',
                'name' => 'KOTA BINJAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => '1277',
                'province_id' => '12',
                'name' => 'KOTA PADANGSIDIMPUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => '1278',
                'province_id' => '12',
                'name' => 'KOTA GUNUNGSITOLI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => '1301',
                'province_id' => '13',
                'name' => 'KABUPATEN KEPULAUAN MENTAWAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => '1302',
                'province_id' => '13',
                'name' => 'KABUPATEN PESISIR SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => '1303',
                'province_id' => '13',
                'name' => 'KABUPATEN SOLOK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => '1304',
                'province_id' => '13',
                'name' => 'KABUPATEN SIJUNJUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => '1305',
                'province_id' => '13',
                'name' => 'KABUPATEN TANAH DATAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => '1306',
                'province_id' => '13',
                'name' => 'KABUPATEN PADANG PARIAMAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => '1307',
                'province_id' => '13',
                'name' => 'KABUPATEN AGAM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => '1308',
                'province_id' => '13',
                'name' => 'KABUPATEN LIMA PULUH KOTA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => '1309',
                'province_id' => '13',
                'name' => 'KABUPATEN PASAMAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => '1310',
                'province_id' => '13',
                'name' => 'KABUPATEN SOLOK SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => '1311',
                'province_id' => '13',
                'name' => 'KABUPATEN DHARMASRAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => '1312',
                'province_id' => '13',
                'name' => 'KABUPATEN PASAMAN BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => '1371',
                'province_id' => '13',
                'name' => 'KOTA PADANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => '1372',
                'province_id' => '13',
                'name' => 'KOTA SOLOK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => '1373',
                'province_id' => '13',
                'name' => 'KOTA SAWAH LUNTO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => '1374',
                'province_id' => '13',
                'name' => 'KOTA PADANG PANJANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => '1375',
                'province_id' => '13',
                'name' => 'KOTA BUKITTINGGI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => '1376',
                'province_id' => '13',
                'name' => 'KOTA PAYAKUMBUH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => '1377',
                'province_id' => '13',
                'name' => 'KOTA PARIAMAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => '1401',
                'province_id' => '14',
                'name' => 'KABUPATEN KUANTAN SINGINGI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => '1402',
                'province_id' => '14',
                'name' => 'KABUPATEN INDRAGIRI HULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => '1403',
                'province_id' => '14',
                'name' => 'KABUPATEN INDRAGIRI HILIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => '1404',
                'province_id' => '14',
                'name' => 'KABUPATEN PELALAWAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => '1405',
                'province_id' => '14',
                'name' => 'KABUPATEN S I A K',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => '1406',
                'province_id' => '14',
                'name' => 'KABUPATEN KAMPAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => '1407',
                'province_id' => '14',
                'name' => 'KABUPATEN ROKAN HULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => '1408',
                'province_id' => '14',
                'name' => 'KABUPATEN BENGKALIS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => '1409',
                'province_id' => '14',
                'name' => 'KABUPATEN ROKAN HILIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => '1410',
                'province_id' => '14',
                'name' => 'KABUPATEN KEPULAUAN MERANTI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => '1471',
                'province_id' => '14',
                'name' => 'KOTA PEKANBARU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => '1473',
                'province_id' => '14',
                'name' => 'KOTA D U M A I',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => '1501',
                'province_id' => '15',
                'name' => 'KABUPATEN KERINCI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => '1502',
                'province_id' => '15',
                'name' => 'KABUPATEN MERANGIN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => '1503',
                'province_id' => '15',
                'name' => 'KABUPATEN SAROLANGUN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => '1504',
                'province_id' => '15',
                'name' => 'KABUPATEN BATANG HARI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => '1505',
                'province_id' => '15',
                'name' => 'KABUPATEN MUARO JAMBI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => '1506',
                'province_id' => '15',
                'name' => 'KABUPATEN TANJUNG JABUNG TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => '1507',
                'province_id' => '15',
                'name' => 'KABUPATEN TANJUNG JABUNG BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => '1508',
                'province_id' => '15',
                'name' => 'KABUPATEN TEBO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => '1509',
                'province_id' => '15',
                'name' => 'KABUPATEN BUNGO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => '1571',
                'province_id' => '15',
                'name' => 'KOTA JAMBI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => '1572',
                'province_id' => '15',
                'name' => 'KOTA SUNGAI PENUH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => '1601',
                'province_id' => '16',
                'name' => 'KABUPATEN OGAN KOMERING ULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => '1602',
                'province_id' => '16',
                'name' => 'KABUPATEN OGAN KOMERING ILIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => '1603',
                'province_id' => '16',
                'name' => 'KABUPATEN MUARA ENIM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => '1604',
                'province_id' => '16',
                'name' => 'KABUPATEN LAHAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => '1605',
                'province_id' => '16',
                'name' => 'KABUPATEN MUSI RAWAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => '1606',
                'province_id' => '16',
                'name' => 'KABUPATEN MUSI BANYUASIN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => '1607',
                'province_id' => '16',
                'name' => 'KABUPATEN BANYU ASIN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => '1608',
                'province_id' => '16',
                'name' => 'KABUPATEN OGAN KOMERING ULU SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => '1609',
                'province_id' => '16',
                'name' => 'KABUPATEN OGAN KOMERING ULU TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => '1610',
                'province_id' => '16',
                'name' => 'KABUPATEN OGAN ILIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => '1611',
                'province_id' => '16',
                'name' => 'KABUPATEN EMPAT LAWANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => '1612',
                'province_id' => '16',
                'name' => 'KABUPATEN PENUKAL ABAB LEMATANG ILIR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => '1613',
                'province_id' => '16',
                'name' => 'KABUPATEN MUSI RAWAS UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => '1671',
                'province_id' => '16',
                'name' => 'KOTA PALEMBANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => '1672',
                'province_id' => '16',
                'name' => 'KOTA PRABUMULIH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => '1673',
                'province_id' => '16',
                'name' => 'KOTA PAGAR ALAM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => '1674',
                'province_id' => '16',
                'name' => 'KOTA LUBUKLINGGAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => '1701',
                'province_id' => '17',
                'name' => 'KABUPATEN BENGKULU SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => '1702',
                'province_id' => '17',
                'name' => 'KABUPATEN REJANG LEBONG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => '1703',
                'province_id' => '17',
                'name' => 'KABUPATEN BENGKULU UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => '1704',
                'province_id' => '17',
                'name' => 'KABUPATEN KAUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => '1705',
                'province_id' => '17',
                'name' => 'KABUPATEN SELUMA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => '1706',
                'province_id' => '17',
                'name' => 'KABUPATEN MUKOMUKO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => '1707',
                'province_id' => '17',
                'name' => 'KABUPATEN LEBONG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => '1708',
                'province_id' => '17',
                'name' => 'KABUPATEN KEPAHIANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => '1709',
                'province_id' => '17',
                'name' => 'KABUPATEN BENGKULU TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => '1771',
                'province_id' => '17',
                'name' => 'KOTA BENGKULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => '1801',
                'province_id' => '18',
                'name' => 'KABUPATEN LAMPUNG BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => '1802',
                'province_id' => '18',
                'name' => 'KABUPATEN TANGGAMUS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => '1803',
                'province_id' => '18',
                'name' => 'KABUPATEN LAMPUNG SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => '1804',
                'province_id' => '18',
                'name' => 'KABUPATEN LAMPUNG TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => '1805',
                'province_id' => '18',
                'name' => 'KABUPATEN LAMPUNG TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => '1806',
                'province_id' => '18',
                'name' => 'KABUPATEN LAMPUNG UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => '1807',
                'province_id' => '18',
                'name' => 'KABUPATEN WAY KANAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => '1808',
                'province_id' => '18',
                'name' => 'KABUPATEN TULANGBAWANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => '1809',
                'province_id' => '18',
                'name' => 'KABUPATEN PESAWARAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => '1810',
                'province_id' => '18',
                'name' => 'KABUPATEN PRINGSEWU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => '1811',
                'province_id' => '18',
                'name' => 'KABUPATEN MESUJI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => '1812',
                'province_id' => '18',
                'name' => 'KABUPATEN TULANG BAWANG BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => '1813',
                'province_id' => '18',
                'name' => 'KABUPATEN PESISIR BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => '1871',
                'province_id' => '18',
                'name' => 'KOTA BANDAR LAMPUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => '1872',
                'province_id' => '18',
                'name' => 'KOTA METRO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => '1901',
                'province_id' => '19',
                'name' => 'KABUPATEN BANGKA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => '1902',
                'province_id' => '19',
                'name' => 'KABUPATEN BELITUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => '1903',
                'province_id' => '19',
                'name' => 'KABUPATEN BANGKA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => '1904',
                'province_id' => '19',
                'name' => 'KABUPATEN BANGKA TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => '1905',
                'province_id' => '19',
                'name' => 'KABUPATEN BANGKA SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => '1906',
                'province_id' => '19',
                'name' => 'KABUPATEN BELITUNG TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => '1971',
                'province_id' => '19',
                'name' => 'KOTA PANGKAL PINANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => '2101',
                'province_id' => '21',
                'name' => 'KABUPATEN KARIMUN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => '2102',
                'province_id' => '21',
                'name' => 'KABUPATEN BINTAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => '2103',
                'province_id' => '21',
                'name' => 'KABUPATEN NATUNA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => '2104',
                'province_id' => '21',
                'name' => 'KABUPATEN LINGGA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => '2105',
                'province_id' => '21',
                'name' => 'KABUPATEN KEPULAUAN ANAMBAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => '2171',
                'province_id' => '21',
                'name' => 'KOTA B A T A M',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => '2172',
                'province_id' => '21',
                'name' => 'KOTA TANJUNG PINANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => '3101',
                'province_id' => '31',
                'name' => 'KABUPATEN KEPULAUAN SERIBU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => '3171',
                'province_id' => '31',
                'name' => 'KOTA JAKARTA SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => '3172',
                'province_id' => '31',
                'name' => 'KOTA JAKARTA TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => '3173',
                'province_id' => '31',
                'name' => 'KOTA JAKARTA PUSAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => '3174',
                'province_id' => '31',
                'name' => 'KOTA JAKARTA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => '3175',
                'province_id' => '31',
                'name' => 'KOTA JAKARTA UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => '3201',
                'province_id' => '32',
                'name' => 'KABUPATEN BOGOR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => '3202',
                'province_id' => '32',
                'name' => 'KABUPATEN SUKABUMI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => '3203',
                'province_id' => '32',
                'name' => 'KABUPATEN CIANJUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => '3204',
                'province_id' => '32',
                'name' => 'KABUPATEN BANDUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => '3205',
                'province_id' => '32',
                'name' => 'KABUPATEN GARUT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => '3206',
                'province_id' => '32',
                'name' => 'KABUPATEN TASIKMALAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => '3207',
                'province_id' => '32',
                'name' => 'KABUPATEN CIAMIS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => '3208',
                'province_id' => '32',
                'name' => 'KABUPATEN KUNINGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => '3209',
                'province_id' => '32',
                'name' => 'KABUPATEN CIREBON',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => '3210',
                'province_id' => '32',
                'name' => 'KABUPATEN MAJALENGKA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => '3211',
                'province_id' => '32',
                'name' => 'KABUPATEN SUMEDANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => '3212',
                'province_id' => '32',
                'name' => 'KABUPATEN INDRAMAYU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => '3213',
                'province_id' => '32',
                'name' => 'KABUPATEN SUBANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => '3214',
                'province_id' => '32',
                'name' => 'KABUPATEN PURWAKARTA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => '3215',
                'province_id' => '32',
                'name' => 'KABUPATEN KARAWANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => '3216',
                'province_id' => '32',
                'name' => 'KABUPATEN BEKASI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => '3217',
                'province_id' => '32',
                'name' => 'KABUPATEN BANDUNG BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => '3218',
                'province_id' => '32',
                'name' => 'KABUPATEN PANGANDARAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => '3271',
                'province_id' => '32',
                'name' => 'KOTA BOGOR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => '3272',
                'province_id' => '32',
                'name' => 'KOTA SUKABUMI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => '3273',
                'province_id' => '32',
                'name' => 'KOTA BANDUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => '3274',
                'province_id' => '32',
                'name' => 'KOTA CIREBON',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => '3275',
                'province_id' => '32',
                'name' => 'KOTA BEKASI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => '3276',
                'province_id' => '32',
                'name' => 'KOTA DEPOK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => '3277',
                'province_id' => '32',
                'name' => 'KOTA CIMAHI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => '3278',
                'province_id' => '32',
                'name' => 'KOTA TASIKMALAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => '3279',
                'province_id' => '32',
                'name' => 'KOTA BANJAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => '3301',
                'province_id' => '33',
                'name' => 'KABUPATEN CILACAP',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => '3302',
                'province_id' => '33',
                'name' => 'KABUPATEN BANYUMAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => '3303',
                'province_id' => '33',
                'name' => 'KABUPATEN PURBALINGGA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => '3304',
                'province_id' => '33',
                'name' => 'KABUPATEN BANJARNEGARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => '3305',
                'province_id' => '33',
                'name' => 'KABUPATEN KEBUMEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => '3306',
                'province_id' => '33',
                'name' => 'KABUPATEN PURWOREJO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => '3307',
                'province_id' => '33',
                'name' => 'KABUPATEN WONOSOBO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => '3308',
                'province_id' => '33',
                'name' => 'KABUPATEN MAGELANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => '3309',
                'province_id' => '33',
                'name' => 'KABUPATEN BOYOLALI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => '3310',
                'province_id' => '33',
                'name' => 'KABUPATEN KLATEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => '3311',
                'province_id' => '33',
                'name' => 'KABUPATEN SUKOHARJO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => '3312',
                'province_id' => '33',
                'name' => 'KABUPATEN WONOGIRI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => '3313',
                'province_id' => '33',
                'name' => 'KABUPATEN KARANGANYAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => '3314',
                'province_id' => '33',
                'name' => 'KABUPATEN SRAGEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => '3315',
                'province_id' => '33',
                'name' => 'KABUPATEN GROBOGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => '3316',
                'province_id' => '33',
                'name' => 'KABUPATEN BLORA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => '3317',
                'province_id' => '33',
                'name' => 'KABUPATEN REMBANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => '3318',
                'province_id' => '33',
                'name' => 'KABUPATEN PATI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => '3319',
                'province_id' => '33',
                'name' => 'KABUPATEN KUDUS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => '3320',
                'province_id' => '33',
                'name' => 'KABUPATEN JEPARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => '3321',
                'province_id' => '33',
                'name' => 'KABUPATEN DEMAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => '3322',
                'province_id' => '33',
                'name' => 'KABUPATEN SEMARANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => '3323',
                'province_id' => '33',
                'name' => 'KABUPATEN TEMANGGUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => '3324',
                'province_id' => '33',
                'name' => 'KABUPATEN KENDAL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => '3325',
                'province_id' => '33',
                'name' => 'KABUPATEN BATANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => '3326',
                'province_id' => '33',
                'name' => 'KABUPATEN PEKALONGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => '3327',
                'province_id' => '33',
                'name' => 'KABUPATEN PEMALANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => '3328',
                'province_id' => '33',
                'name' => 'KABUPATEN TEGAL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => '3329',
                'province_id' => '33',
                'name' => 'KABUPATEN BREBES',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => '3371',
                'province_id' => '33',
                'name' => 'KOTA MAGELANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => '3372',
                'province_id' => '33',
                'name' => 'KOTA SURAKARTA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => '3373',
                'province_id' => '33',
                'name' => 'KOTA SALATIGA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => '3374',
                'province_id' => '33',
                'name' => 'KOTA SEMARANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => '3375',
                'province_id' => '33',
                'name' => 'KOTA PEKALONGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => '3376',
                'province_id' => '33',
                'name' => 'KOTA TEGAL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => '3401',
                'province_id' => '34',
                'name' => 'KABUPATEN KULON PROGO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => '3402',
                'province_id' => '34',
                'name' => 'KABUPATEN BANTUL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => '3403',
                'province_id' => '34',
                'name' => 'KABUPATEN GUNUNG KIDUL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => '3404',
                'province_id' => '34',
                'name' => 'KABUPATEN SLEMAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => '3471',
                'province_id' => '34',
                'name' => 'KOTA YOGYAKARTA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => '3501',
                'province_id' => '35',
                'name' => 'KABUPATEN PACITAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => '3502',
                'province_id' => '35',
                'name' => 'KABUPATEN PONOROGO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => '3503',
                'province_id' => '35',
                'name' => 'KABUPATEN TRENGGALEK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => '3504',
                'province_id' => '35',
                'name' => 'KABUPATEN TULUNGAGUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => '3505',
                'province_id' => '35',
                'name' => 'KABUPATEN BLITAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => '3506',
                'province_id' => '35',
                'name' => 'KABUPATEN KEDIRI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => '3507',
                'province_id' => '35',
                'name' => 'KABUPATEN MALANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => '3508',
                'province_id' => '35',
                'name' => 'KABUPATEN LUMAJANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => '3509',
                'province_id' => '35',
                'name' => 'KABUPATEN JEMBER',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => '3510',
                'province_id' => '35',
                'name' => 'KABUPATEN BANYUWANGI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => '3511',
                'province_id' => '35',
                'name' => 'KABUPATEN BONDOWOSO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => '3512',
                'province_id' => '35',
                'name' => 'KABUPATEN SITUBONDO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => '3513',
                'province_id' => '35',
                'name' => 'KABUPATEN PROBOLINGGO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => '3514',
                'province_id' => '35',
                'name' => 'KABUPATEN PASURUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => '3515',
                'province_id' => '35',
                'name' => 'KABUPATEN SIDOARJO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => '3516',
                'province_id' => '35',
                'name' => 'KABUPATEN MOJOKERTO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => '3517',
                'province_id' => '35',
                'name' => 'KABUPATEN JOMBANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => '3518',
                'province_id' => '35',
                'name' => 'KABUPATEN NGANJUK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => '3519',
                'province_id' => '35',
                'name' => 'KABUPATEN MADIUN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => '3520',
                'province_id' => '35',
                'name' => 'KABUPATEN MAGETAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => '3521',
                'province_id' => '35',
                'name' => 'KABUPATEN NGAWI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => '3522',
                'province_id' => '35',
                'name' => 'KABUPATEN BOJONEGORO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => '3523',
                'province_id' => '35',
                'name' => 'KABUPATEN TUBAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => '3524',
                'province_id' => '35',
                'name' => 'KABUPATEN LAMONGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => '3525',
                'province_id' => '35',
                'name' => 'KABUPATEN GRESIK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => '3526',
                'province_id' => '35',
                'name' => 'KABUPATEN BANGKALAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => '3527',
                'province_id' => '35',
                'name' => 'KABUPATEN SAMPANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => '3528',
                'province_id' => '35',
                'name' => 'KABUPATEN PAMEKASAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => '3529',
                'province_id' => '35',
                'name' => 'KABUPATEN SUMENEP',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => '3571',
                'province_id' => '35',
                'name' => 'KOTA KEDIRI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => '3572',
                'province_id' => '35',
                'name' => 'KOTA BLITAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => '3573',
                'province_id' => '35',
                'name' => 'KOTA MALANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => '3574',
                'province_id' => '35',
                'name' => 'KOTA PROBOLINGGO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => '3575',
                'province_id' => '35',
                'name' => 'KOTA PASURUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => '3576',
                'province_id' => '35',
                'name' => 'KOTA MOJOKERTO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => '3577',
                'province_id' => '35',
                'name' => 'KOTA MADIUN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => '3578',
                'province_id' => '35',
                'name' => 'KOTA SURABAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => '3579',
                'province_id' => '35',
                'name' => 'KOTA BATU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => '3601',
                'province_id' => '36',
                'name' => 'KABUPATEN PANDEGLANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => '3602',
                'province_id' => '36',
                'name' => 'KABUPATEN LEBAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => '3603',
                'province_id' => '36',
                'name' => 'KABUPATEN TANGERANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => '3604',
                'province_id' => '36',
                'name' => 'KABUPATEN SERANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => '3671',
                'province_id' => '36',
                'name' => 'KOTA TANGERANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => '3672',
                'province_id' => '36',
                'name' => 'KOTA CILEGON',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => '3673',
                'province_id' => '36',
                'name' => 'KOTA SERANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => '3674',
                'province_id' => '36',
                'name' => 'KOTA TANGERANG SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => '5101',
                'province_id' => '51',
                'name' => 'KABUPATEN JEMBRANA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => '5102',
                'province_id' => '51',
                'name' => 'KABUPATEN TABANAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => '5103',
                'province_id' => '51',
                'name' => 'KABUPATEN BADUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => '5104',
                'province_id' => '51',
                'name' => 'KABUPATEN GIANYAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => '5105',
                'province_id' => '51',
                'name' => 'KABUPATEN KLUNGKUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => '5106',
                'province_id' => '51',
                'name' => 'KABUPATEN BANGLI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => '5107',
                'province_id' => '51',
                'name' => 'KABUPATEN KARANG ASEM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => '5108',
                'province_id' => '51',
                'name' => 'KABUPATEN BULELENG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => '5171',
                'province_id' => '51',
                'name' => 'KOTA DENPASAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => '5201',
                'province_id' => '52',
                'name' => 'KABUPATEN LOMBOK BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => '5202',
                'province_id' => '52',
                'name' => 'KABUPATEN LOMBOK TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => '5203',
                'province_id' => '52',
                'name' => 'KABUPATEN LOMBOK TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => '5204',
                'province_id' => '52',
                'name' => 'KABUPATEN SUMBAWA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => '5205',
                'province_id' => '52',
                'name' => 'KABUPATEN DOMPU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => '5206',
                'province_id' => '52',
                'name' => 'KABUPATEN BIMA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => '5207',
                'province_id' => '52',
                'name' => 'KABUPATEN SUMBAWA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => '5208',
                'province_id' => '52',
                'name' => 'KABUPATEN LOMBOK UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => '5271',
                'province_id' => '52',
                'name' => 'KOTA MATARAM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => '5272',
                'province_id' => '52',
                'name' => 'KOTA BIMA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => '5301',
                'province_id' => '53',
                'name' => 'KABUPATEN SUMBA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => '5302',
                'province_id' => '53',
                'name' => 'KABUPATEN SUMBA TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => '5303',
                'province_id' => '53',
                'name' => 'KABUPATEN KUPANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => '5304',
                'province_id' => '53',
                'name' => 'KABUPATEN TIMOR TENGAH SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => '5305',
                'province_id' => '53',
                'name' => 'KABUPATEN TIMOR TENGAH UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => '5306',
                'province_id' => '53',
                'name' => 'KABUPATEN BELU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => '5307',
                'province_id' => '53',
                'name' => 'KABUPATEN ALOR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => '5308',
                'province_id' => '53',
                'name' => 'KABUPATEN LEMBATA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => '5309',
                'province_id' => '53',
                'name' => 'KABUPATEN FLORES TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => '5310',
                'province_id' => '53',
                'name' => 'KABUPATEN SIKKA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => '5311',
                'province_id' => '53',
                'name' => 'KABUPATEN ENDE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => '5312',
                'province_id' => '53',
                'name' => 'KABUPATEN NGADA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => '5313',
                'province_id' => '53',
                'name' => 'KABUPATEN MANGGARAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => '5314',
                'province_id' => '53',
                'name' => 'KABUPATEN ROTE NDAO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => '5315',
                'province_id' => '53',
                'name' => 'KABUPATEN MANGGARAI BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => '5316',
                'province_id' => '53',
                'name' => 'KABUPATEN SUMBA TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => '5317',
                'province_id' => '53',
                'name' => 'KABUPATEN SUMBA BARAT DAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => '5318',
                'province_id' => '53',
                'name' => 'KABUPATEN NAGEKEO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => '5319',
                'province_id' => '53',
                'name' => 'KABUPATEN MANGGARAI TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => '5320',
                'province_id' => '53',
                'name' => 'KABUPATEN SABU RAIJUA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => '5321',
                'province_id' => '53',
                'name' => 'KABUPATEN MALAKA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => '5371',
                'province_id' => '53',
                'name' => 'KOTA KUPANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => '6101',
                'province_id' => '61',
                'name' => 'KABUPATEN SAMBAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => '6102',
                'province_id' => '61',
                'name' => 'KABUPATEN BENGKAYANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => '6103',
                'province_id' => '61',
                'name' => 'KABUPATEN LANDAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => '6104',
                'province_id' => '61',
                'name' => 'KABUPATEN MEMPAWAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => '6105',
                'province_id' => '61',
                'name' => 'KABUPATEN SANGGAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => '6106',
                'province_id' => '61',
                'name' => 'KABUPATEN KETAPANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => '6107',
                'province_id' => '61',
                'name' => 'KABUPATEN SINTANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => '6108',
                'province_id' => '61',
                'name' => 'KABUPATEN KAPUAS HULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => '6109',
                'province_id' => '61',
                'name' => 'KABUPATEN SEKADAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => '6110',
                'province_id' => '61',
                'name' => 'KABUPATEN MELAWI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => '6111',
                'province_id' => '61',
                'name' => 'KABUPATEN KAYONG UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => '6112',
                'province_id' => '61',
                'name' => 'KABUPATEN KUBU RAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => '6171',
                'province_id' => '61',
                'name' => 'KOTA PONTIANAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => '6172',
                'province_id' => '61',
                'name' => 'KOTA SINGKAWANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => '6201',
                'province_id' => '62',
                'name' => 'KABUPATEN KOTAWARINGIN BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => '6202',
                'province_id' => '62',
                'name' => 'KABUPATEN KOTAWARINGIN TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => '6203',
                'province_id' => '62',
                'name' => 'KABUPATEN KAPUAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => '6204',
                'province_id' => '62',
                'name' => 'KABUPATEN BARITO SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => '6205',
                'province_id' => '62',
                'name' => 'KABUPATEN BARITO UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => '6206',
                'province_id' => '62',
                'name' => 'KABUPATEN SUKAMARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => '6207',
                'province_id' => '62',
                'name' => 'KABUPATEN LAMANDAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => '6208',
                'province_id' => '62',
                'name' => 'KABUPATEN SERUYAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => '6209',
                'province_id' => '62',
                'name' => 'KABUPATEN KATINGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => '6210',
                'province_id' => '62',
                'name' => 'KABUPATEN PULANG PISAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => '6211',
                'province_id' => '62',
                'name' => 'KABUPATEN GUNUNG MAS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => '6212',
                'province_id' => '62',
                'name' => 'KABUPATEN BARITO TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => '6213',
                'province_id' => '62',
                'name' => 'KABUPATEN MURUNG RAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => '6271',
                'province_id' => '62',
                'name' => 'KOTA PALANGKA RAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => '6301',
                'province_id' => '63',
                'name' => 'KABUPATEN TANAH LAUT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => '6302',
                'province_id' => '63',
                'name' => 'KABUPATEN KOTA BARU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => '6303',
                'province_id' => '63',
                'name' => 'KABUPATEN BANJAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => '6304',
                'province_id' => '63',
                'name' => 'KABUPATEN BARITO KUALA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => '6305',
                'province_id' => '63',
                'name' => 'KABUPATEN TAPIN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => '6306',
                'province_id' => '63',
                'name' => 'KABUPATEN HULU SUNGAI SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => '6307',
                'province_id' => '63',
                'name' => 'KABUPATEN HULU SUNGAI TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => '6308',
                'province_id' => '63',
                'name' => 'KABUPATEN HULU SUNGAI UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => '6309',
                'province_id' => '63',
                'name' => 'KABUPATEN TABALONG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => '6310',
                'province_id' => '63',
                'name' => 'KABUPATEN TANAH BUMBU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => '6311',
                'province_id' => '63',
                'name' => 'KABUPATEN BALANGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => '6371',
                'province_id' => '63',
                'name' => 'KOTA BANJARMASIN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => '6372',
                'province_id' => '63',
                'name' => 'KOTA BANJAR BARU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => '6401',
                'province_id' => '64',
                'name' => 'KABUPATEN PASER',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => '6402',
                'province_id' => '64',
                'name' => 'KABUPATEN KUTAI BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => '6403',
                'province_id' => '64',
                'name' => 'KABUPATEN KUTAI KARTANEGARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => '6404',
                'province_id' => '64',
                'name' => 'KABUPATEN KUTAI TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => '6405',
                'province_id' => '64',
                'name' => 'KABUPATEN BERAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => '6409',
                'province_id' => '64',
                'name' => 'KABUPATEN PENAJAM PASER UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => '6411',
                'province_id' => '64',
                'name' => 'KABUPATEN MAHAKAM HULU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => '6471',
                'province_id' => '64',
                'name' => 'KOTA BALIKPAPAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => '6472',
                'province_id' => '64',
                'name' => 'KOTA SAMARINDA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => '6474',
                'province_id' => '64',
                'name' => 'KOTA BONTANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => '6501',
                'province_id' => '65',
                'name' => 'KABUPATEN MALINAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => '6502',
                'province_id' => '65',
                'name' => 'KABUPATEN BULUNGAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => '6503',
                'province_id' => '65',
                'name' => 'KABUPATEN TANA TIDUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => '6504',
                'province_id' => '65',
                'name' => 'KABUPATEN NUNUKAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => '6571',
                'province_id' => '65',
                'name' => 'KOTA TARAKAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => '7101',
                'province_id' => '71',
                'name' => 'KABUPATEN BOLAANG MONGONDOW',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => '7102',
                'province_id' => '71',
                'name' => 'KABUPATEN MINAHASA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => '7103',
                'province_id' => '71',
                'name' => 'KABUPATEN KEPULAUAN SANGIHE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => '7104',
                'province_id' => '71',
                'name' => 'KABUPATEN KEPULAUAN TALAUD',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => '7105',
                'province_id' => '71',
                'name' => 'KABUPATEN MINAHASA SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => '7106',
                'province_id' => '71',
                'name' => 'KABUPATEN MINAHASA UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => '7107',
                'province_id' => '71',
                'name' => 'KABUPATEN BOLAANG MONGONDOW UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => '7108',
                'province_id' => '71',
                'name' => 'KABUPATEN SIAU TAGULANDANG BIARO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => '7109',
                'province_id' => '71',
                'name' => 'KABUPATEN MINAHASA TENGGARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => '7110',
                'province_id' => '71',
                'name' => 'KABUPATEN BOLAANG MONGONDOW SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => '7111',
                'province_id' => '71',
                'name' => 'KABUPATEN BOLAANG MONGONDOW TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => '7171',
                'province_id' => '71',
                'name' => 'KOTA MANADO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => '7172',
                'province_id' => '71',
                'name' => 'KOTA BITUNG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => '7173',
                'province_id' => '71',
                'name' => 'KOTA TOMOHON',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => '7174',
                'province_id' => '71',
                'name' => 'KOTA KOTAMOBAGU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => '7201',
                'province_id' => '72',
                'name' => 'KABUPATEN BANGGAI KEPULAUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => '7202',
                'province_id' => '72',
                'name' => 'KABUPATEN BANGGAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => '7203',
                'province_id' => '72',
                'name' => 'KABUPATEN MOROWALI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => '7204',
                'province_id' => '72',
                'name' => 'KABUPATEN POSO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => '7205',
                'province_id' => '72',
                'name' => 'KABUPATEN DONGGALA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => '7206',
                'province_id' => '72',
                'name' => 'KABUPATEN TOLI-TOLI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => '7207',
                'province_id' => '72',
                'name' => 'KABUPATEN BUOL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => '7208',
                'province_id' => '72',
                'name' => 'KABUPATEN PARIGI MOUTONG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => '7209',
                'province_id' => '72',
                'name' => 'KABUPATEN TOJO UNA-UNA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => '7210',
                'province_id' => '72',
                'name' => 'KABUPATEN SIGI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => '7211',
                'province_id' => '72',
                'name' => 'KABUPATEN BANGGAI LAUT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => '7212',
                'province_id' => '72',
                'name' => 'KABUPATEN MOROWALI UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => '7271',
                'province_id' => '72',
                'name' => 'KOTA PALU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => '7301',
                'province_id' => '73',
                'name' => 'KABUPATEN KEPULAUAN SELAYAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => '7302',
                'province_id' => '73',
                'name' => 'KABUPATEN BULUKUMBA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => '7303',
                'province_id' => '73',
                'name' => 'KABUPATEN BANTAENG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => '7304',
                'province_id' => '73',
                'name' => 'KABUPATEN JENEPONTO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => '7305',
                'province_id' => '73',
                'name' => 'KABUPATEN TAKALAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => '7306',
                'province_id' => '73',
                'name' => 'KABUPATEN GOWA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => '7307',
                'province_id' => '73',
                'name' => 'KABUPATEN SINJAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => '7308',
                'province_id' => '73',
                'name' => 'KABUPATEN MAROS',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => '7309',
                'province_id' => '73',
                'name' => 'KABUPATEN PANGKAJENE DAN KEPULAUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => '7310',
                'province_id' => '73',
                'name' => 'KABUPATEN BARRU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => '7311',
                'province_id' => '73',
                'name' => 'KABUPATEN BONE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => '7312',
                'province_id' => '73',
                'name' => 'KABUPATEN SOPPENG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => '7313',
                'province_id' => '73',
                'name' => 'KABUPATEN WAJO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => '7314',
                'province_id' => '73',
                'name' => 'KABUPATEN SIDENRENG RAPPANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => '7315',
                'province_id' => '73',
                'name' => 'KABUPATEN PINRANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => '7316',
                'province_id' => '73',
                'name' => 'KABUPATEN ENREKANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => '7317',
                'province_id' => '73',
                'name' => 'KABUPATEN LUWU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => '7318',
                'province_id' => '73',
                'name' => 'KABUPATEN TANA TORAJA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => '7322',
                'province_id' => '73',
                'name' => 'KABUPATEN LUWU UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => '7325',
                'province_id' => '73',
                'name' => 'KABUPATEN LUWU TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => '7326',
                'province_id' => '73',
                'name' => 'KABUPATEN TORAJA UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => '7371',
                'province_id' => '73',
                'name' => 'KOTA MAKASSAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => '7372',
                'province_id' => '73',
                'name' => 'KOTA PAREPARE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => '7373',
                'province_id' => '73',
                'name' => 'KOTA PALOPO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => '7401',
                'province_id' => '74',
                'name' => 'KABUPATEN BUTON',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => '7402',
                'province_id' => '74',
                'name' => 'KABUPATEN MUNA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => '7403',
                'province_id' => '74',
                'name' => 'KABUPATEN KONAWE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => '7404',
                'province_id' => '74',
                'name' => 'KABUPATEN KOLAKA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => '7405',
                'province_id' => '74',
                'name' => 'KABUPATEN KONAWE SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => '7406',
                'province_id' => '74',
                'name' => 'KABUPATEN BOMBANA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => '7407',
                'province_id' => '74',
                'name' => 'KABUPATEN WAKATOBI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => '7408',
                'province_id' => '74',
                'name' => 'KABUPATEN KOLAKA UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => '7409',
                'province_id' => '74',
                'name' => 'KABUPATEN BUTON UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => '7410',
                'province_id' => '74',
                'name' => 'KABUPATEN KONAWE UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => '7411',
                'province_id' => '74',
                'name' => 'KABUPATEN KOLAKA TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => '7412',
                'province_id' => '74',
                'name' => 'KABUPATEN KONAWE KEPULAUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => '7413',
                'province_id' => '74',
                'name' => 'KABUPATEN MUNA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => '7414',
                'province_id' => '74',
                'name' => 'KABUPATEN BUTON TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => '7415',
                'province_id' => '74',
                'name' => 'KABUPATEN BUTON SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => '7471',
                'province_id' => '74',
                'name' => 'KOTA KENDARI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => '7472',
                'province_id' => '74',
                'name' => 'KOTA BAUBAU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => '7501',
                'province_id' => '75',
                'name' => 'KABUPATEN BOALEMO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => '7502',
                'province_id' => '75',
                'name' => 'KABUPATEN GORONTALO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => '7503',
                'province_id' => '75',
                'name' => 'KABUPATEN POHUWATO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => '7504',
                'province_id' => '75',
                'name' => 'KABUPATEN BONE BOLANGO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => '7505',
                'province_id' => '75',
                'name' => 'KABUPATEN GORONTALO UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => '7571',
                'province_id' => '75',
                'name' => 'KOTA GORONTALO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => '7601',
                'province_id' => '76',
                'name' => 'KABUPATEN MAJENE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => '7602',
                'province_id' => '76',
                'name' => 'KABUPATEN POLEWALI MANDAR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => '7603',
                'province_id' => '76',
                'name' => 'KABUPATEN MAMASA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => '7604',
                'province_id' => '76',
                'name' => 'KABUPATEN MAMUJU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => '7605',
                'province_id' => '76',
                'name' => 'KABUPATEN MAMUJU UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => '7606',
                'province_id' => '76',
                'name' => 'KABUPATEN MAMUJU TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => '8101',
                'province_id' => '81',
                'name' => 'KABUPATEN MALUKU TENGGARA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => '8102',
                'province_id' => '81',
                'name' => 'KABUPATEN MALUKU TENGGARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => '8103',
                'province_id' => '81',
                'name' => 'KABUPATEN MALUKU TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => '8104',
                'province_id' => '81',
                'name' => 'KABUPATEN BURU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => '8105',
                'province_id' => '81',
                'name' => 'KABUPATEN KEPULAUAN ARU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => '8106',
                'province_id' => '81',
                'name' => 'KABUPATEN SERAM BAGIAN BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => '8107',
                'province_id' => '81',
                'name' => 'KABUPATEN SERAM BAGIAN TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => '8108',
                'province_id' => '81',
                'name' => 'KABUPATEN MALUKU BARAT DAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => '8109',
                'province_id' => '81',
                'name' => 'KABUPATEN BURU SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => '8171',
                'province_id' => '81',
                'name' => 'KOTA AMBON',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => '8172',
                'province_id' => '81',
                'name' => 'KOTA TUAL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => '8201',
                'province_id' => '82',
                'name' => 'KABUPATEN HALMAHERA BARAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => '8202',
                'province_id' => '82',
                'name' => 'KABUPATEN HALMAHERA TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => '8203',
                'province_id' => '82',
                'name' => 'KABUPATEN KEPULAUAN SULA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => '8204',
                'province_id' => '82',
                'name' => 'KABUPATEN HALMAHERA SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => '8205',
                'province_id' => '82',
                'name' => 'KABUPATEN HALMAHERA UTARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => '8206',
                'province_id' => '82',
                'name' => 'KABUPATEN HALMAHERA TIMUR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => '8207',
                'province_id' => '82',
                'name' => 'KABUPATEN PULAU MOROTAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => '8208',
                'province_id' => '82',
                'name' => 'KABUPATEN PULAU TALIABU',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => '8271',
                'province_id' => '82',
                'name' => 'KOTA TERNATE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => '8272',
                'province_id' => '82',
                'name' => 'KOTA TIDORE KEPULAUAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => '9101',
                'province_id' => '91',
                'name' => 'KABUPATEN FAKFAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => '9102',
                'province_id' => '91',
                'name' => 'KABUPATEN KAIMANA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => '9103',
                'province_id' => '91',
                'name' => 'KABUPATEN TELUK WONDAMA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => '9104',
                'province_id' => '91',
                'name' => 'KABUPATEN TELUK BINTUNI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => '9105',
                'province_id' => '91',
                'name' => 'KABUPATEN MANOKWARI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => '9106',
                'province_id' => '91',
                'name' => 'KABUPATEN SORONG SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => '9107',
                'province_id' => '91',
                'name' => 'KABUPATEN SORONG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => '9108',
                'province_id' => '91',
                'name' => 'KABUPATEN RAJA AMPAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => '9109',
                'province_id' => '91',
                'name' => 'KABUPATEN TAMBRAUW',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => '9110',
                'province_id' => '91',
                'name' => 'KABUPATEN MAYBRAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => '9111',
                'province_id' => '91',
                'name' => 'KABUPATEN MANOKWARI SELATAN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => '9112',
                'province_id' => '91',
                'name' => 'KABUPATEN PEGUNUNGAN ARFAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => '9171',
                'province_id' => '91',
                'name' => 'KOTA SORONG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => '9401',
                'province_id' => '94',
                'name' => 'KABUPATEN MERAUKE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => '9402',
                'province_id' => '94',
                'name' => 'KABUPATEN JAYAWIJAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => '9403',
                'province_id' => '94',
                'name' => 'KABUPATEN JAYAPURA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => '9404',
                'province_id' => '94',
                'name' => 'KABUPATEN NABIRE',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => '9408',
                'province_id' => '94',
                'name' => 'KABUPATEN KEPULAUAN YAPEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => '9409',
                'province_id' => '94',
                'name' => 'KABUPATEN BIAK NUMFOR',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => '9410',
                'province_id' => '94',
                'name' => 'KABUPATEN PANIAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => '9411',
                'province_id' => '94',
                'name' => 'KABUPATEN PUNCAK JAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => '9412',
                'province_id' => '94',
                'name' => 'KABUPATEN MIMIKA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => '9413',
                'province_id' => '94',
                'name' => 'KABUPATEN BOVEN DIGOEL',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => '9414',
                'province_id' => '94',
                'name' => 'KABUPATEN MAPPI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => '9415',
                'province_id' => '94',
                'name' => 'KABUPATEN ASMAT',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => '9416',
                'province_id' => '94',
                'name' => 'KABUPATEN YAHUKIMO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => '9417',
                'province_id' => '94',
                'name' => 'KABUPATEN PEGUNUNGAN BINTANG',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => '9418',
                'province_id' => '94',
                'name' => 'KABUPATEN TOLIKARA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => '9419',
                'province_id' => '94',
                'name' => 'KABUPATEN SARMI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '9420',
                'province_id' => '94',
                'name' => 'KABUPATEN KEEROM',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '9426',
                'province_id' => '94',
                'name' => 'KABUPATEN WAROPEN',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '9427',
                'province_id' => '94',
                'name' => 'KABUPATEN SUPIORI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '9428',
                'province_id' => '94',
                'name' => 'KABUPATEN MAMBERAMO RAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '9429',
                'province_id' => '94',
                'name' => 'KABUPATEN NDUGA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '9430',
                'province_id' => '94',
                'name' => 'KABUPATEN LANNY JAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '9431',
                'province_id' => '94',
                'name' => 'KABUPATEN MAMBERAMO TENGAH',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '9432',
                'province_id' => '94',
                'name' => 'KABUPATEN YALIMO',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '9433',
                'province_id' => '94',
                'name' => 'KABUPATEN PUNCAK',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '9434',
                'province_id' => '94',
                'name' => 'KABUPATEN DOGIYAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '9435',
                'province_id' => '94',
                'name' => 'KABUPATEN INTAN JAYA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '9436',
                'province_id' => '94',
                'name' => 'KABUPATEN DEIYAI',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '9471',
                'province_id' => '94',
                'name' => 'KOTA JAYAPURA',
                'meta' => NULL,
                'created_at' => '2019-10-05 13:57:28',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}