<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Driver::class, 200)->create();
        factory(App\Valet::class, 200)->create();
        factory(App\Vehicle::class, 200)->create();
        factory(App\Inquery::class, 200)->create();
        $this->call(StatusTableSeeder::class);
        $this->call(VillagesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
    }
}
