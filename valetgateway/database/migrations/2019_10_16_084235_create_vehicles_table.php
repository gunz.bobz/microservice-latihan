<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_pemilik',255);
            $table->string('foto_kendaraan',255);
            $table->string('jenis_kendaraan',255);
            $table->string('warna_kendaraan',255);
            $table->string('merk_kendaraan',255);
            $table->string('nomor_polisi',255);
            $table->string('tipe_kendaraan',255);
            $table->string('tahun_kendaraan',4);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
