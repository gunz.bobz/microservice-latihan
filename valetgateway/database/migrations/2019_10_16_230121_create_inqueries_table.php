<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInqueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inqueries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('inquery_no',10)->nullable();
            $table->string('barcode',10)->nullable();
            $table->string('id_customer',10);
            $table->string('nama_kendaraan',255);
            $table->string('warna_kendaraan',255);
            $table->string('nomor_polisi',255);
            $table->string('status',255)->default('1');
            $table->timestamps();
            $table->dateTime('completed_at')->nullable();
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inqueries');
    }
}
