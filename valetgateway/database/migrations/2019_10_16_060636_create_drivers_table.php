<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor',10)->unique();
            $table->string('nama',70);
            // $table->string('nomor',9)->unique()->default('concat(year(now()),lpad(month(now()),2,0),lpad(id, 3, 0))');
            $table->string('foto',191)->default('user.png');
            $table->date('tanggallahir');
            $table->string('jk',20);
            $table->string('alamat',80);
            $table->string('villages',80);
            $table->string('districts',80);
            $table->string('cities',80);
            $table->string('provinces',80);
            $table->string('telp',80)->unique();
            $table->string('email',60)->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
