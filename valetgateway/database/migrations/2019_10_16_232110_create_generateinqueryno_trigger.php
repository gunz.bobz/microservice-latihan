<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenerateinquerynoTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER `generatenomorinquery` BEFORE INSERT ON `inqueries` FOR EACH ROW SET NEW.completed_at = NEW.created_at, NEW.inquery_no = concat(day(NEW.created_at),(SELECT count(*)+1 FROM inqueries where date(created_at)=curdate())), NEW.barcode = concat(year(now()),lpad(month(now()),2,0),(SELECT LPAD(AUTO_INCREMENT,4,0) FROM `information_schema`.`TABLES` where TABLE_SCHEMA="valet" and TABLE_NAME="inqueries"))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `generatenomorinquery`');
        
    }
}
