<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
   $router->post('register', 'AuthController@register');

     // Matches "/api/login
    $router->post('login', 'AuthController@login');
});

//history
$router->get('/history/{id_driver}', 'History\HistoryController@show');


//get valet capacity data
$router->get('/usedvalet', 'Inquery\InqueryController@usedvalet');
$router->get('/totalvalet', 'Inquery\InqueryController@totalvalet');
$router->get('/availablevalet', 'Inquery\InqueryController@availablevalet');

//inquery show requested booking valet
$router->get('/inquery/{inquery}', 'Inquery\InqueryController@show');
//inquery request a booking valet
$router->post('/inquery/', 'Inquery\InqueryController@store');
//barcode scan 
$router->put('/inquery/{barcode}', 'Inquery\InqueryController@barcodescan');
//cancel booking
$router->put('/inquery/{barcode}/cancel', 'Inquery\InqueryController@cancel');
//cancel checkout
$router->put('/inquery/{barcode}/checkout', 'Inquery\InqueryController@checkout');
//barcode scan 
$router->put('/scan/{barcode}', 'Inquery\InqueryController@barcodescan');

//valet confirm 
$router->post('/valet/{barcode}', 'Valet\ValetController@confirm');

//driver
$router->get('/driver/{driver}', 'Driver\DriverController@show');
$router->post('/driver/', 'Driver\DriverController@store');

//menu
$router->get('api/menus', 'Menu\MenuController@index');
$router->put('api/menus/{menus}', 'Menu\MenuController@update');
// $router->delete('api/menus/{menus}', 'Menu\MenuController@destroy');
// $router->delete('api/valets/{menus}', 'valet\ValetController@destroy');
$router->post('api/menus/create', 'Menu\MenuController@store');
$router->get('api/menus/{menus}', 'Menu\MenuController@show');
// $router->post('/driver/', 'Driver\DriverController@store');

