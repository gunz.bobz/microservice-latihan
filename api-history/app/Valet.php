<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Valet extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    
    public function valet()
    {
        return $this->hasMany('App\Inquery', 'barcode', 'barcode_valet');
    }
    // public function History()
    // {
    //     return $this->hasOneThrough('App\Status','App\Inquery');
    // }




    // protected $hidden = ['deleted_at'];
    public function history()
    {
        return $this->hasmanyThrough(
            'App\Status',
            'App\Inquery',
            'status_id', // Foreign key on users table...
            'id', // Foreign key on posts table...
            'barcode_valet', // Local key on countries table...
            'barcode' // Local key on users table...
        );
    }
}
